var mapOptions = {
	//Coordinates of Bengaluru city (India)
	center : [12.9716, 77.5946],
	zoom   : 10
}

//Creating Map objects
var map = new L.map('map', mapOptions);

//Creating a Later objects
var layer = new L.TileLayer('https://korona.geog.uni-heidelberg.de/tiles/roads/x={x}&y={y}&z={z}');

var layer2 = new L.TileLayer('https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}');

//Adding default ESRI layer to map
map.addLayer(layer2);


//Adding Layers to map
var baselayers = {
          "Korona" : layer,
          "GoogleEarth" : layer2

 };

L.control.layers(baselayers).addTo(map);

//Creating marker options
    var markerOptions = {
        title: "MyLocation",
        clickable: true,
        draggable: true
    }

//Creating a Marker
var marker = L.marker([13.0129, 77.7181], markerOptions);


//Adding marker to the map
marker.addTo(map);

//Add bindpopup to the marker
marker.bindPopup("Welcome to Bengaluru !!!!").openPopup();
marker.addTo(map);
